Link Flair Commenter
====================

Installations
-------------

This script requires python 3, praw, and praw-oauth2util.

To install praw type in your command line:

    pip install praw
    pip install praw-oauth2util

Reddit OAuth Setup
------------------

You will need to created a reddit app to be able to run this (https://www.reddit.com/prefs/apps/).
When you created it, select "Personal Use Script" from the options, and set the `redirect_uri` to `http://127.0.0.1:65010/authorize_callback`. All the other options are up to you to fill in. After that you will need to get the
app key and app secret from the same page and add them to the corresponding variables in the oauth.txt file.

Config
------

Add the subreddit you want it to find posts from to the SUBREDDIT variable (don't include the /r/). 
Add the class name of the link flair you want it to reply to, to the LINK_FLAIR_CLASS variable.
And add the comment you want it to reply with inbetween the """ """ on the COMMENT variable, like so:

    COMMENT = """
    This is what it will reply with.\n\n
    This will be on another line. You need the \n\n
    To break the lines for Reddit markdown.
    """ 