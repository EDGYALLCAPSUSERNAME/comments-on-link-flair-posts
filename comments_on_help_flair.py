import time
import praw
import OAuth2Util

SUBREDDIT = ''
LINK_FLAIR_CLASS = ''
COMMENT = """

"""


def find_help_posts(r, o):
    submission_stream = praw.helpers.submission_stream(r, SUBREDDIT)
    print("Searching for posts...")
    for submission in submission_stream:
        if submission.link_flair_css_class == LINK_FLAIR_CLASS:
            print('Commenting on post...')
            submission.add_comment(COMMENT)


def main():
    r = praw.Reddit(user_agent='linkflaircomment v1.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    while True:
        try:
            find_help_posts(r, o)
        except Exception as e:
            print('ERROR: {}'.format(e))
            time.sleep(360)


if __name__ == "__main__":
    main()
